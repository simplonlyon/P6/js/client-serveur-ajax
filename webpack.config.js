module.exports = {
    mode: 'development',
    entry: {
        index: './src/index.js',
        promise: './src/promise.js'
    },
    output: {
       filename: "./[name].js"
    },
    devtool: 'inline-source-map'
};