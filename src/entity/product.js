export class Product {

    constructor(name, description, price, id = null) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.id = id;
        this.quantity = 1;
    }

    toHTML() {

    }

}