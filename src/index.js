import { ProductService } from "./service/product-service";
//On fait une instance du service
let product = new ProductService();
//On appelle sa méthode findAll qui renvoie une Promise.
//On doit donc faire un .then() pour accéder aux données de celle ci
product.findAll().then(function(data){
    console.log(data);
});